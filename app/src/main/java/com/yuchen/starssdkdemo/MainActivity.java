package com.yuchen.starssdkdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yuchen.starssdk.MqttServiceCallBack;
import com.yuchen.starssdk.RegistOnIotPlatformCallBack;
import com.yuchen.starssdk.StarsConfig;
import com.yuchen.starssdk.StarsSdkHelper;
import com.yuchen.starssdk.mqtt.MqttMessageReceiveCommon;
import com.yuchen.starssdk.mqtt.PubTopic;
import com.yuchen.starssdk.utils.LogUtils;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    public void gatewayHttp(View v){
        Intent intent = new Intent(this,RegisterHttpGatewayActivity.class);
        startActivity(intent);
    }
    public void gatewayMqtt(View v){
        Intent intent = new Intent(this,RegisterMqttGatewayActivity.class);
        startActivity(intent);
    }
    public void directHttp(View v){
        Intent intent = new Intent(this,RegisterHttpDirectActivity.class);
        startActivity(intent);
    }
    public void directMqtt(View v){
        Intent intent = new Intent(this,RegisterMqttDirectActivity.class);
        startActivity(intent);
    }
}