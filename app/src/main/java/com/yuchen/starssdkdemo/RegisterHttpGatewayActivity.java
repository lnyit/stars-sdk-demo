package com.yuchen.starssdkdemo;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yuchen.starssdk.MqttServiceCallBack;
import com.yuchen.starssdk.RegistOnIotPlatformCallBack;
import com.yuchen.starssdk.StarsConfig;
import com.yuchen.starssdk.StarsSdkHelper;
import com.yuchen.starssdk.utils.LogUtils;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class RegisterHttpGatewayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final StarsConfig config = new StarsConfig(StarsConfig.DeviceType.GatWay, IotConfigConstant.tenantId, IotConfigConstant.productId, IotConfigConstant.productKey, new int[]{4},new String[]{ IotConfigConstant.subProductId});
//        final StarsConfig config = new StarsConfig(StarsConfig.DeviceType.Direct, "098279385", "cp32471080638", "yc8bo9a7pc7abr13ff");
        config.setDebug(true);
//        config.setProductName("mgmonitor");
//        config.setHeartBeatDuration(30000);
//        config.setDeviceName("123");
        StarsSdkHelper.getInstence().init(this, config);


        StarsSdkHelper.getInstence().registOnIotPlatform(new RegistOnIotPlatformCallBack() {
            @Override
            public void onSuccess(String jsonStr) {
                LogUtils.d(jsonStr);
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg);
            }
        });

        StarsSdkHelper.getInstence().setMqttServiceCallBack(new MqttServiceCallBack() {
            @Override
            public void connectSuccess() {
                LogUtils.d("connectSuccess");
//                StarsSdkHelper.getInstence().registOnIotPlatform();
            }

            @Override
            public void connectFail(Throwable e) {
                LogUtils.d("connectFail " + e.getMessage());
            }

            @Override
            public void connectLost(Throwable e) {
                LogUtils.d("connectLost " + e.getMessage());
            }

            @Override
            public void disconnected() {
                LogUtils.d("disconnected ");
            }

            @Override
            public void messageDeliveryComplete(IMqttDeliveryToken arg0) {
                try {
                    LogUtils.d("messageDeliveryComplete: " + new String(arg0.getMessage().getPayload()));
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {

            }
        });

        StarsSdkHelper.getInstence().connectMqtt();
    }
}