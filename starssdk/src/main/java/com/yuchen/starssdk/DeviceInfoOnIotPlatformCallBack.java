package com.yuchen.starssdk;

public interface DeviceInfoOnIotPlatformCallBack {

    void onSuccess(String jsonStr);

    void onError(String msg);
}
