package com.yuchen.starssdk;

import android.os.Build;

import com.alibaba.fastjson.JSON;
import com.yuchen.starssdk.mqtt.PubTopic;
import com.yuchen.starssdk.net.API;
import com.yuchen.starssdk.utils.DeviceUtil;
import com.yuchen.starssdk.utils.LogUtils;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.HashMap;
import java.util.Map;

public class StarsConfig {

    private static final String TAG = "StarsConfig";
    private boolean isDebug = true;

    private String deviceSn = DeviceUtil.getDeviceSn();
    private String deviceId;
    private String deviceToken;
    private String osName = "android";
    private String productName = "pname";
    private String tenantId;
    private String customerId;
    private String mqttUrl;
    private String mqttUserName;
    private String mqttPassword;
    private String additionalInfo;
    private boolean isGroup = true;
    private Map<String, Integer> subscribeTopics;
    private REGISTER_WAY registerWay = REGISTER_WAY.HTTP;

    private long heartDuration;


    private String registUrl = API.directDevDynamicRegister;
    private String changeUrl = API.directDevDynamicRegister;
    private String detailUrl = API.directDevDetail;
    private String deviceName = "设备-" + Build.MODEL + "-" + DeviceUtil.getDeviceSn();
    private String deviceAlias = "设备-" + Build.MODEL + "-" + DeviceUtil.getDeviceSn();
    private String productId;
    private String productKey;
    private String deviceInfoJson;

    private int[] subProductCounts;
    private String[] subProductIds;

    private DeviceType deviceType;

    private long heartBeatDuration = 30 * 1000;

    public StarsConfig() {
        init();
    }


    public enum DeviceType {
        Direct("直连设备", 1), GatWay("网关设备", 2);

        private String name;
        private int value;

        DeviceType(String name, int value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }


    }

    public enum REGISTER_WAY {
        HTTP("HTTP", 1), MQTT("MQTT", 2);

        private String name;
        private int value;

        REGISTER_WAY(String name, int value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }


    public StarsConfig(DeviceType deviceType, String deviceToken, String productName, String tenantId, String registUrl, String deviceName, String deviceAlias, String productId, String productKey, int[] subProductCounts, String[] subProductIds) {
        this.deviceType = deviceType;
        this.registUrl = registUrl;
        this.deviceName = deviceName;
        this.deviceAlias = deviceAlias;
        this.productId = productId;
        this.productKey = productKey;
        this.subProductCounts = subProductCounts;
        this.subProductIds = subProductIds;

        this.deviceToken = deviceToken;
        this.productName = productName;
        this.tenantId = tenantId;
        init();

    }

    public StarsConfig(DeviceType deviceType, String deviceToken, String productName, String tenantId, String customerId, String registUrl, String deviceName, String deviceAlias, String productId, String productKey, int[] subProductCounts, String[] subProductIds) {
        this.deviceType = deviceType;
        this.registUrl = registUrl;
        this.deviceName = deviceName;
        this.deviceAlias = deviceAlias;
        this.productId = productId;
        this.productKey = productKey;
        this.subProductCounts = subProductCounts;
        this.subProductIds = subProductIds;
        this.deviceToken = deviceToken;
        this.productName = productName;
        this.tenantId = tenantId;
        this.customerId = customerId;
        init();

    }

    public StarsConfig(DeviceType deviceType, String tenantId, String productId, String productKey) {
        this.deviceType = deviceType;
        this.tenantId = tenantId;
        this.productId = productId;
        this.productKey = productKey;
        init();
    }

    public StarsConfig(DeviceType deviceType, String tenantId, String customerId, String productId, String productKey) {
        this.deviceType = deviceType;
        this.tenantId = tenantId;
        this.productId = productId;
        this.productKey = productKey;
        this.customerId = customerId;
        init();
    }


    public StarsConfig(DeviceType deviceType, String tenantId, String productId, String productKey, int[] subProductCounts, String[] subProductIds) {
        this.deviceType = deviceType;
        this.tenantId = tenantId;
        this.productId = productId;
        this.productKey = productKey;
        this.subProductCounts = subProductCounts;
        this.subProductIds = subProductIds;
        init();
    }

    public StarsConfig(DeviceType deviceType, String tenantId, String customerId, String productId, String productKey, int[] subProductCount, String[] subProductId) {
        this.deviceType = deviceType;
        this.tenantId = tenantId;
        this.customerId = customerId;
        this.productId = productId;
        this.productKey = productKey;
        this.subProductCounts = subProductCounts;
        this.subProductIds = subProductIds;
        init();
    }


    private void init() {
        LogUtils.debug = isDebug;
        subscribeTopics = new HashMap<>();
//        this.mqttUrl = "tcp://device.starsaiot.com:1883";//"tcp://39.98.61.38:1883";

//        if (isDebug){
//            if (DeviceType.Direct == this.deviceType){
//                this.registUrl = API.directDevDynamicRegisterDebug;
//                this.detailUrl = API.directDevDetailDebug;
//            }else if (DeviceType.GatWay== this.deviceType){
//                this.registUrl = API.gatewayDynamicRegisterDebug;
//                this.detailUrl = API.gatewayDevDetailDebug;
//            }
//            this.changeUrl = API.gatewayChangeDebug;
//
//            this.mqttUrl = "tcp://39.98.61.38:1883";
//            this.mqttUserName = "hercules";
//            this.mqttPassword = "hercules";
//        }else{
        if (DeviceType.Direct == this.deviceType) {
            this.registUrl = API.directDevDynamicRegister;
            this.detailUrl = API.directDevDetail;
        } else if (DeviceType.GatWay == this.deviceType) {
            this.registUrl = API.gatewayDynamicRegister;
            this.detailUrl = API.gatewayDevDetail;
        }
        this.changeUrl = API.gatewayChange;
        this.mqttUrl = "tcp://device.starsaiot.com:1883";
        this.mqttUserName = "hercules";
        this.mqttPassword = "hercules";
//        }

    }

    public void doSubscribeTopics() {
        subscribeTopics.clear();
        //  /sys/platform/bunsmsg/${tenantId}/${业务系统名}/${deviceId}
//        String topic = "/sys/platform/bunsmsg/"+tenantId+"/"+productName+"/"+deviceId+"/"+new PartitionNumUtils().getPartitionNum(tenantId);
        String topic = "/sys/platform/bunsmsg/" + tenantId + "/" + productName + "/" + deviceId;
        subscribeTopics.put(topic, 1);
        subscribeTopics.put("/device/cmd/business/" + deviceId, 1);
        subscribeTopics.put("/device/cmd/business/app/" + deviceId, 1);
        subscribeTopics.put("/device/cmd/business/mcu/" + deviceId, 1);
        LogUtils.d(TAG,"doSubscribeTopics: "+ JSON.toJSONString(subscribeTopics));
    }


    public String getChangeUrl() {
        return changeUrl;
    }

    public void setChangeUrl(String changeUrl) {
        this.changeUrl = changeUrl;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public void setDebug(boolean debug) {
        isDebug = debug;
        init();
    }

    public long getHeartBeatDuration() {
        return heartBeatDuration;
    }

    public void setHeartBeatDuration(long heartBeatDuration) {
        if (heartBeatDuration < 20 * 1000) {
            LogUtils.e(TAG, "heartBeatDuration 最小值为 20000");
            heartBeatDuration = 20 * 1000;
        }
        this.heartBeatDuration = heartBeatDuration;
    }

    public String getDeviceInfoJson() {
        return deviceInfoJson;
    }

    public void setDeviceInfoJson(String deviceInfoJson) {
        this.deviceInfoJson = deviceInfoJson;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getRegistUrl() {
        return registUrl;
    }

    public void setRegistUrl(String registUrl) {
        this.registUrl = registUrl;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceAlias() {
        return deviceAlias;
    }

    public void setDeviceAlias(String deviceAlias) {
        this.deviceAlias = deviceAlias;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }

    public int[] getSubProductCounts() {
        return subProductCounts;
    }

    public void setSubProductCount(int[] subProductCounts) {
        this.subProductCounts = subProductCounts;
    }

    public String[] getSubProductIds() {
        return subProductIds;
    }

    public void setSubProductId(String[] subProductIds) {
        this.subProductIds = subProductIds;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Map<String, Integer> getSubscribeTopics() {
        return subscribeTopics;
    }

    public void setSubscribeTopics(Map<String, Integer> subscribeTopics) {
        this.subscribeTopics = subscribeTopics;
    }

    public long getHeartDuration() {
        return heartDuration;
    }

    public String getClientId() {
        return "device" + "_" + productName + "_" + deviceSn;
    }


    public void setHeartDuration(long heartDuration) {
        this.heartDuration = heartDuration;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        doSubscribeTopics();
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getMqttUrl() {
        return mqttUrl;
    }

    public void setMqttUrl(String mqttUrl) {
        this.mqttUrl = mqttUrl;
    }

    public String getMqttUserName() {
        return mqttUserName;
    }

    public void setMqttUserName(String mqttUserName) {
        this.mqttUserName = mqttUserName;
    }

    public String getMqttPassword() {
        return mqttPassword;
    }

    public void setMqttPassword(String mqttPassword) {
        this.mqttPassword = mqttPassword;
    }

    public REGISTER_WAY getRegisterWay() {
        return registerWay;
    }

    public void setRegisterWay(REGISTER_WAY registerWay) {
        this.registerWay = registerWay;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public boolean setMqttClientTopics(MqttAndroidClient client) {
        LogUtils.d(TAG, subscribeTopics.toString());
        int[] qoes = new int[subscribeTopics.size()];
        String[] topics = new String[subscribeTopics.size()];
        int i = 0;
        for (Map.Entry<String, Integer> entry : subscribeTopics.entrySet()) {
            qoes[i] = entry.getValue();
            topics[i] = entry.getKey();
            i++;
        }
        try {
            client.subscribe(topics, qoes);
            return true;
        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean setMqttClientRegisterTopic(MqttAndroidClient client){
        int[] qoes = new int[]{2};
        String[] topics = new String[]{"/device/register/business/"+getDeviceSn()};
        LogUtils.d(TAG,"register reponse topic： "+"/device/register/business/"+getDeviceSn());
        try {
            client.subscribe(topics, qoes);
            return true;
        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }


}
