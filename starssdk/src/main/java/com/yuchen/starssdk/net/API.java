package com.yuchen.starssdk.net;

public interface API {


    //网关设备注册
    String gatewayDynamicRegister = "http://device.starsaiot.com:9600/hercules/open/api/v1/device/business/gateway/dynamicRegister";
    //直链设备注册
    String directDevDynamicRegister = "http://device.starsaiot.com:9600/hercules/open/api/v1/device/business/directDev/dynamicRegister";
    //网关设备获取设备详情-包含子设备
    String gatewayDevDetail = "http://device.starsaiot.com:9600/hercules/open/api/v1/device/business/gateway/getDetailNew";
    //    String gatewayDevDetail = "http://device.starsaiot.com:9600/hercules/open/api/v1/device/business/gateway/getDetail";
    //直链设备获取设备详情
    String directDevDetail = "http://device.starsaiot.com:9600/hercules/open/api/v1/device/business/directDev/getDeviceDetail";

    String gatewayChange = "http://device.starsaiot.com:9600/hercules/open/api/v1/device/business/gateway/change";





    String gatewayDynamicRegisterDebug = "http://iot.iperfumetech.com:9600/hercules/open/api/v1/device/business/gateway/dynamicRegister";
    String directDevDynamicRegisterDebug = "http://iot.iperfumetech.com:9600/hercules/open/api/v1/device/business/directDev/dynamicRegister";
    String gatewayChangeDebug = "http://iot.iperfumetech.com:9600/hercules/open/api/v1/device/business/gateway/change";
    String gatewayDevDetailDebug = "http://iot.iperfumetech.com:9600/hercules/open/api/v1/device/business/gateway/getDetailNew";
    String directDevDetailDebug = "http://iot.iperfumetech.com:9600/hercules/open/api/v1/device/business/directDev/getDeviceDetail";

}
